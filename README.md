# Thalassa-js

## Infos

- Auteur: Sylvain Lagrue [sylvain.lagrue@utc.fr](mailto:sylvain.lagrue@utc.fr)
- Licence code : [GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.fr.html)
- Licence sujet : [CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/fr/)

