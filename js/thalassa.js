/*******************************************************/
/* Thalassa.js                                         */
/* Author : Sylvain Lagrue <sylvain.lagrue@hds.utc.fr> */
/* Licence : GPL3                                      */
/*******************************************************/


/******* Model *******/

let segments = [
  [
    [[180,190],[160,180]],[[160,180],[160,170]],[[160,170],[200,160]],[[200,160],[200,180]],[[200,180],[190,210]],[[190,210],[150,210]],[[150,210],[130,180]],[[130,180],[140,140]],[[140,140],[170,130]],[[170,130],[200,130]],[[200,130],[230,170]],[[230,170],[220,200]],[[220,200],[200,230]],[[200,230],[170,230]],[[170,230],[130,230]],[[130,230],[100,200]],[[100,200],[90,180]],[[90,180],[90,140]],[[90,140],[110,120]],[[110,120],[150,80]],[[150,80],[200,80]],[[200,80],[240,90]],[[240,90],[260,120]],[[260,120],[290,190]],[[290,190],[260,230]],[[260,230],[220,260]],[[220,260],[130,280]],[[130,280],[50,260]],[[50,260],[0,170]],[[0,170],[10,90]],[[10,90],[50,50]],[[50,50],[120,10]],[[120,10],[170,0]],[[170,0],[220,0]],[[220,0],[220,80]]
  ],
  [
    [[0,220],[420,220]],[[90,220],[50,190]],[[50,190],[310,180]],[[310,180],[270,220]],[[220,180],[220,140]],[[220,140],[290,140]],[[290,140],[290,180]],[[160,180],[160,20]],[[160,20],[240,70]],[[240,70],[160,140]],[[160,20],[100,100]],[[100,100],[160,140]],[[230,140],[230,130]],[[250,140],[250,130]],[[280,140],[280,130]],[[220,130],[290,130]],[[240,150],[240,160]],[[240,160],[250,160]],[[250,160],[250,150]],[[250,150],[240,150]],[[300,190],[300,220]],[[300,200],[280,220]],[[50,190],[30,170]],[[280,50],[300,80]],[[300,80],[320,50]],[[20,30],[30,50]],[[30,50],[50,30]],[[350,0],[350,30]],[[350,30],[370,40]],[[370,40],[400,40]],[[400,40],[410,30]],[[410,30],[420,20]],[[330,10],[300,20]],[[360,50],[350,70]],[[390,50],[400,80]]
  ]
];
let lastClick = [null, null];


function average(src1, src2, dst, alpha = 0.5) {
  let i;

  segments[dst] = [];

  if (segments[src1].length === 0) {
    segments[src1].push([
      [0, 0],
      [0, 0]
    ]);
  } else if (segments[src2].length === 0) {
    segments[src2].push([
      [0, 0],
      [0, 0]
    ]);
  }

  let delta = segments[src1].length - segments[src2].length;

  if (delta > 0) {
    for (i = 0; i < delta; i += 1) {
      segments[src2].push([segments[src2][0][0], segments[src2][0][0]]);
    }
  } else if (delta < 0) {
    for (i = 0; i < -delta; i += 1) {
      segments[src1].push([segments[src1][0][0], segments[src1][0][0]]);
    }
  }

  let x1, y1, x2, y2;
  for (i = 0; i < segments[src1].length; i += 1) {
    x1 = Math.round(segments[src1][i][0][0] * alpha + segments[src2][i][0][0] * (1 - alpha));
    y1 = Math.round(segments[src1][i][0][1] * alpha + segments[src2][i][0][1] * (1 - alpha));

    x2 = Math.round(segments[src1][i][1][0] * alpha + segments[src2][i][1][0] * (1 - alpha));
    y2 = Math.round(segments[src1][i][1][1] * alpha + segments[src2][i][1][1] * (1 - alpha));

    segments[dst].push([
      [x1, y1],
      [x2, y2]
    ]);
  }
}


function deepClone(obj) {
  let k;

  if (typeof obj !== 'object') {
    return obj;
  }

  let res = [];
  for (k in Object.keys(obj)) {
    res[k] = deepClone(obj[k]);
  }

  return res;
}


function move(src, dst) {
  segments[dst] = deepClone(segments[src]);
}


function saveSegment(s, n = 0) {
  segments[n].push(s);
}


function mirror(width, n = 0) {
  let i;
  let p0, p1;

  for (i = 0; i < segments[n].length; i += 1) {
    p0 = [width - segments[n][i][0][0], segments[n][i][0][1]];
    p1 = [width - segments[n][i][1][0], segments[n][i][1][1]];

    segments[n][i] = [p0, p1];
  }
}


/*******  View  *******/

function drawLine(ctx, p1, p2) {
  ctx.lineWidth = 5;
  ctx.strokeStyle = 'grey';
  ctx.lineCap = 'round';

  ctx.beginPath();
  ctx.moveTo(p1[0], p1[1]);
  ctx.lineTo(p2[0], p2[1]);
  ctx.stroke();
}


function drawSegments(canvas, n = 0) {
  let i;
  let ctx = canvas.getContext('2d');

  for (i = 0; i < segments[n].length; i += 1) {
    drawLine(ctx, segments[n][i][0], segments[n][i][1]);
  }
}


function clear(canvas) {
  let ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}


function paint() {
  let canvas1 = document.getElementById('canvas1');
  clear(canvas1);
  drawSegments(canvas1, 0);

  let canvas2 = document.getElementById('canvas2');
  clear(canvas2);
  drawSegments(canvas2, 1);
}


/******* Controller *******/

function computeCoordinates(e, grid = 10) {
  let canvas = e.currentTarget;
  let rect = canvas.getBoundingClientRect();

  let x = Math.round((e.clientX - rect.left) / grid) * grid;
  let y = Math.round((e.clientY - rect.top) / grid) * grid;

  return [x, y];
}


function treatClick(e, n) {
  let canvas = e.currentTarget;
  let ctx = canvas.getContext('2d');

  if (lastClick[n] === null || e.shiftKey) {
    lastClick[n] = computeCoordinates(e);
  } else {
    let c = computeCoordinates(e, 10);
    drawLine(ctx, lastClick[n], c);
    saveSegment([lastClick[n], c], n);

    lastClick[n] = c;
  }
}


function doUndo(e, n) {
  lastClick[n] = segments[n].pop()[0];
  paint();
}


function doClear(e, n) {
  segments[n] = [];
  lastClick[n] = null;

  clear(document.getElementById('canvas3'));
  clear(document.getElementById('canvas4'));

  paint();
}


function doExport(e, n) {
  let textArea = document.getElementById('json' + (n + 1));
  textArea.value = JSON.stringify(segments[n]);
}


function doImport(e, n) {
  let textArea = document.getElementById('json' + (n + 1));
  segments[n] = JSON.parse(textArea.value);
  paint();
}


function doMirror(e, n) {
  let canvas = document.getElementById('canvas' + (n + 1));
  mirror(canvas.width, n);
  paint();
}


function doMerge(e) {
  let canvas3 = e.currentTarget;
  let i;

  let nbFrames = 100;

  let anim = function(k) {
    return function() {
      average(0, 1, 2, 1 - k / nbFrames);
      clear(canvas3);
      drawSegments(canvas3, 2);
    };
  };

  for (i = 0; i <= nbFrames; i += 1) {
    window.setTimeout(anim(i), i * 25);
  }

  canvas3.removeEventListener('click', doMerge);
  canvas3.addEventListener('click', doMerge2, false);
}


function doMerge2(e) {
  let canvas4 = e.currentTarget;
  let i;

  let nbFrames = 100;

  let anim = function(k) {
    return function() {
      average(1, 0, 2, 1 - k / nbFrames);
      clear(canvas4);
      drawSegments(canvas4, 2);
    };
  };

  for (i = 0; i <= nbFrames; i += 1) {
    window.setTimeout(anim(i), i * 25);
  }

  canvas4.removeEventListener('click', doMerge2);
  canvas4.addEventListener('click', doMerge, false);
}

function doMove(e, n) {
  let src = n;
  let dst = (n + 1) % 2;

  move(src, dst);
  paint();
}


/* init */

(function() {
  let canvas1 = document.getElementById('canvas1');
  canvas1.addEventListener('click', (e) => treatClick(e, 0), false);
  window.addEventListener('keypress', (e) => doUndo(e, 0), false);

  let canvas2 = document.getElementById('canvas2');
  canvas2.addEventListener('click', (e) => treatClick(e, 1), false);

  let canvas3 = document.getElementById('canvas3');
  canvas3.addEventListener('click', doMerge, false);

  let canvas4 = document.getElementById('canvas4');
  canvas4.addEventListener('click', doMerge2, false);


  let button;

  button = document.getElementById('clear1');
  button.addEventListener('click', (e) => doClear(e, 0), false);

  button = document.getElementById('clear2');
  button.addEventListener('click', (e) => doClear(e, 1), false);

  button = document.getElementById('undo1');
  button.addEventListener('click', (e) => doUndo(e, 0), false);

  button = document.getElementById('undo2');
  button.addEventListener('click', (e) => doUndo(e, 1), false);

  button = document.getElementById('mirror1');
  button.addEventListener('click', (e) => doMirror(e, 0), false);

  button = document.getElementById('mirror2');
  button.addEventListener('click', (e) => doMirror(e, 1), false);

  button = document.getElementById('move1');
  button.addEventListener('click', (e) => doMove(e, 0), false);

  button = document.getElementById('move2');
  button.addEventListener('click', (e) => doMove(e, 1), false);

  button = document.getElementById('export1');
  button.addEventListener('click', (e) => doExport(e, 0), false);

  button = document.getElementById('export2');
  button.addEventListener('click', (e) => doExport(e, 1), false);

  button = document.getElementById('import1');
  button.addEventListener('click', (e) => doImport(e, 0), false);

  button = document.getElementById('import2');
  button.addEventListener('click', (e) => doImport(e, 1), false);

  paint();
}());
